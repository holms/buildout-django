# WARNING: Tab sensetive file!

.PHONY: clean all
all: develop

develop: clean bootstrap buildout-dev 
prod: clean bootstrap buildout-prod

virtualenv: warning
	virtualenv --no-site-packages .

bootstrap:
	-mkdir eggs
	curl -O http://downloads.buildout.org/2/bootstrap.py
	-python bootstrap.py

buildout-prod:
	bin/buildout -c buildout.cfg -N

buildout-dev:
	bin/buildout -c develop.cfg -N

warning:
	@echo "-------------------------------------------------------"
	@echo "Don't forget to activate virtualenv: . bin/activate"
	@echo "-------------------------------------------------------"

clean:
	-rm -rf parts
	-rm -rf downloads
	-rm -rf src
	-rm -rf local
	-rm -rf bin
	-rm -rf include
	-rm -rf lib
	-rm -rf build
	-rm -rf log
	-rm -rf share
	-rm -rf eggs
	-rm -rf bootstrap.py
	-rm -rf develop-eggs
	-rm -rf .installed.cfg

syncdb:
	bin/django syncdb --all --noinput
	bin/django migrate --fake
	bin/django loaddata initial_data.json

run:
	bin/django runserver 0.0.0.0:8000

test:
	bin/django test $(TESTS)

todo:
	@egrep -n 'FIXME|TODO' $$(find apps -iname '*.py' ; \
                                  find project -iname '*.py')

flake8:
	@bin/flake8 \
            project/

coverage:
		bin/coverage run $(COVERAGE_INCLUDES) bin/django test $(TESTS)
		bin/coverage html -d var/htmlcov/ $(COVERAGE_INCLUDES)
		bin/coverage report $(COVERAGE_INCLUDES)
		@echo "Also try to open var/htmlcov/index.html"

graph:
	bin/django graph_models \
        --group-models \
        --all-applications \
        -o var/graph.png
	if [ "$$(uname)" = "Darwin" ]; then \
            open var/graph.png; \
        else \
            xdg-open var/graph.png; \
        fi

