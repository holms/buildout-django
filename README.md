Buildout under Virtualenv
===============================================

Debian Requirements:
-------------

* python-dev
* curl
* virtualenv
* virtualenvwrapper
* graphviz
* pip install -U distribute
 
Freebsd Requirements:
-------------

* graphics/graphviz
* graphics/png
* graphics/jpeg
* devel/py-pip
* pip install -U distribute
* devel/py-virtualenv

Just do this:

    :::bash
    make virtualenv
    . bin/activate
    make develop

To start over:

    :::bash
    make clean

Before launching django, copy project/settings/credentials.example.py as credentials.py and edit credentials to database
